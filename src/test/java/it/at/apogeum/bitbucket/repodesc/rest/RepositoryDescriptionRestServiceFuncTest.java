package it.at.apogeum.bitbucket.repodesc.rest;

import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import at.apogeum.bitbucket.repodesc.rest.data.DescriptionsData;

public class RepositoryDescriptionRestServiceFuncTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    @Ignore
    public void messageIsValid() {

        String baseUrl = System.getProperty("baseurl");
        String resourceUrl = baseUrl + "/rest/repodesc/1.0/message";

        RestClient client = new RestClient();
        Resource resource = client.resource(resourceUrl);

        DescriptionsData message = resource.get(DescriptionsData.class);

        //assertEquals("wrong message","Hello World",message.getMessage());
    }
}
